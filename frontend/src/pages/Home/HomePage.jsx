import React, { useEffect, useState } from "react";

import moment from "moment";
// import { Zoom } from "@material-ui/core";
import Collapse from "@material-ui/core/Collapse";

import useHttpClient from "../../shared/Hooks/useHttpClient";

import "./HomePage.css";

function HomePage(props) {
  let content = {
    EN: {
      title: "I'm khaled",
      description: "a software developer",
    },
    DE: {
      title: "Ich bin Khaled",
      description: "ein Software Entwickler",
    },
  };
  content = props.language === "EN" ? content.EN : content.DE;

  const home_page_post_id = "613a817ffcb8d050e3828120";
  const { isLoading, sendRequest } = useHttpClient();
  const [clientInfo, setClientInfo] = useState();
  const [likesTotal, setLikesTotal] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        // const response = await axios.get("https://geolocation-db.com/json/");
        const response = await sendRequest("https://geolocation-db.com/json/");
        // console.log(response);        
        setClientInfo(response);
      } catch (err) {}
    };
    fetchData();
  }, [sendRequest]);

  if (!isLoading && clientInfo) {
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await sendRequest(
          `${process.env.REACT_APP_BACKEND_URL}/ratings/${home_page_post_id}`
        );
        // console.log(response);
        setLikesTotal(response.ratings.length);
      } catch (err) {}
    };
    fetchData();
  }, [sendRequest]);

  const eh_like_click = async(event) => {
    
    try {
      await sendRequest(
        `${process.env.REACT_APP_BACKEND_URL}/ratings`,
        "POST",
        JSON.stringify({
          IPv4: clientInfo.IPv4,
          IPv6: "",
          comment: "",
          city : clientInfo.city || "",
          country_code: clientInfo.country_code || "", //"IR"
          country_name: clientInfo.country_name || "", //"Iran"
          latitude: clientInfo.latitude || "", //35.6961,
          longitude: clientInfo.longitude || "", //51.4231,
          postal: clientInfo.postal || "",
          state: clientInfo.state || "", //null,
          rating_type: "like", // "like"
          created_date: moment().format('YYYY-MM-DD'),
          modified_date: moment().format('YYYY-MM-DD'),
          postId: home_page_post_id,
        }),
        { "Content-Type": "Application/json" }
      );
      setLikesTotal(prev=>(prev||0)+1);
    } catch (err) {}
  };

  return (
    <React.Fragment>
      <div
        className="top_container"
        style={{
          backgroundImage: `url(${
            process.env.PUBLIC_URL + "images/homepage/himmel.png"
          })`,
        }}
      >
        <h1 className="title">{content.title}</h1>
        <h3 className="title_description">{content.description}</h3>
        {/* <Zoom in={true}> */}
          <div className={`title_like zoom ${likesTotal && "is_liked"}`} onClick={eh_like_click}>
            <i className={`fas fa-heart ${likesTotal && "fa-2x"}`}></i>
            <span>{likesTotal && likesTotal}</span>
          </div>
        {/* </Zoom> */}
        <div
          className="sonne"
          style={{
            backgroundImage: `url(${
              process.env.PUBLIC_URL + "/images/homepage/sonne.png"
            })`,
          }}
        ></div>
        <img
          className="wolke1"
          src={process.env.PUBLIC_URL + "/images/homepage/wolke3.png"}
          alt=""
        />
        <img
          className="wolke2"
          src={process.env.PUBLIC_URL + "images/homepage/wolke4.png"}
          alt=""
        />
        <img
          className="wolke3"
          src={process.env.PUBLIC_URL + "images/homepage/wolke3.png"}
          alt=""
        />
        <hr className="hr_normal" />
      </div>
      <div
        className="middle_container"
        style={{
          backgroundImage: `url(${
            process.env.PUBLIC_URL + "images/homepage/Boden.png"
          })`,
        }}
      >
        <img
          className="azaditurm"
          src={process.env.PUBLIC_URL + "images/homepage/azadi.png"}
          alt=""
        />
        <img
          className="brandenburg"
          src={process.env.PUBLIC_URL + "images/homepage/brandenburg2.png"}
          alt=""
        />
        <Collapse in={true} style={{ transitionDelay: "2s" }}>
          <img
            className="baum1"
            src={process.env.PUBLIC_URL + "images/homepage/baum1.png"}
            alt=""
          />
        </Collapse>
        <Collapse in={true} style={{ transitionDelay: "1s" }}>
          <img
            className="baum2"
            src={process.env.PUBLIC_URL + "images/homepage/baum2.png"}
            alt=""
          />
        </Collapse>
        <Collapse in={true} style={{ transitionDelay: "3s" }}>
          <img
            className="baum4"
            src={process.env.PUBLIC_URL + "images/homepage/baum2.png"}
            alt=""
          />
        </Collapse>
        <img
          className="baum5"
          src={process.env.PUBLIC_URL + "images/homepage/baum4.png"}
          alt=""
        />
      </div>
    </React.Fragment>
  );
}

export default HomePage;
